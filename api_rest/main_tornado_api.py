from api_rest.utils_tornado import utils_tornado
import sys

from api_rest.servicos import hello_world
from api_rest.servicos import ping
from api_rest.servicos import alive
from api_rest.servicos import login
from api_rest.servicos import logout
if __name__ == "__main__":

    server = utils_tornado.ServidorTornado(
        {
            "/user/login": login.Login,
            "/user/logout": logout.Logout,
            "/api/hello": hello_world.HelloWorld,
            "/api/ping": ping.Ping,
            "/api/alive": alive.Alive,
        },
        arquivo_configuracoes=sys.argv[1]
    )
    server.inicia_servidor()
