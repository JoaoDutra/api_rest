#!/usr/bin/env python
# -*- coding: UTF-8 -*-

u"""
"""

"""
# IMPORTS
"""


import os
import json
import socket
try:
    import configparser
except:
    import ConfigParser as configparser

# Dependencias Tornado
import tornado.web
import tornado.ioloop

# Dependencias PIP
import celery
import motor.motor_tornado
from aredis import StrictRedis, StrictRedisCluster
import re

regex_valor = re.compile('^\$[a-zA-Z0-9_-]+$')


# Todo cirar o proprio WrapConfig

class VariavelDeAmbienteNaoDefinida(Exception):
    pass

class WrapConfig(object):

    def __init__(self, config):
        self._config = config

    def _get_env_var(self, valor):
        if regex_valor.match(str(valor).strip()):
            valor = str(valor).strip()[1:]
            if valor not in os.environ:
                raise VariavelDeAmbienteNaoDefinida(valor)
            else:
                return os.environ[valor]
        else:
            return valor

    def has_option(self, secao, chave):
        if secao in self._config._sections:
            if chave in self._config._sections[secao]:
                return True
        return False

    @property
    def _sections(self):
        return self._config._sections

    def getnone(self, secao, chave):
        try:
            if chave is None:
                return self._config._sections[secao]
            else:
                return self._get_env_var(self._config.get(secao, chave))
        except VariavelDeAmbienteNaoDefinida as varenv:
            return None
        except configparser.NoSectionError:
            return None
        except configparser.NoOptionError:
            return None
        except KeyError:
            return None

    def get(self, secao, chave):
        return self.__call__(secao, chave)

    def getint(self, secao, chave):
        return int(self.__call__(secao, chave))

    def __call__(self, secao, chave=None):
        try:
            if chave is None:
                return self._config._sections[secao]
            else:
                return self._get_env_var(self._config.get(secao, chave))
        except VariavelDeAmbienteNaoDefinida as varenv:
            raise KeyError(f'VARIAVEL DE AMBIENTE {varenv} (secao {secao}, chave {chave}) NAO DEFINIDA')
        except configparser.NoSectionError:
            return KeyError(f'CONFIGURACAO NAO POSSUI SECAO {secao}')
        except configparser.NoOptionError:
            raise KeyError(f'SECAO {secao} NAO TEM CHAVE {chave}')
        except KeyError:
            return self


def le_configuracoes(arquivo_configuracoes):

    # Lemos o arquivo de configurações
    try:
        config = configparser.ConfigParser()
        with open(arquivo_configuracoes) as f:
            config.read_file(f)
        return WrapConfig(config)
    except IOError as ioerr:
        raise IOError("NAO FOI POSSIVEL LER O ARQUIVO {} - {}".format(arquivo_configuracoes, ioerr))

def inicializa_conexao_redis_single(configuracoes):
    try:
        return StrictRedis(configuracoes.get("REDIS", "single"), decode_responses=True)
    except KeyError as kerr:
        raise Handler_Error(f"Arquivo de configuracoes nao contem opcao REDIS.single! - {kerr}")

def inicializa_conexao_redis_cluster(configuracoes):
    try:
        # Identificamos os Nodes
        nodes = str(configuracoes.get("REDIS", "cluster")).strip()
        if '[' in nodes:
            if nodes.startswith('\\'):
                nodes = eval(os.environ['URI_REDIS_CLUSTER'][1:].strip())
            else:
                nodes = json.loads(nodes)

            # Resolvemos o DNS caso tenhamos uma variável de ambiente que isso especifica
            if str(os.environ.get('RESOLVER_DNS_REDIS_CLUSTER', 'false')).lower().strip() in [
                'true', 't', 'verdadeiro', 'v', 'sim', 's', 'yes', 'y', '1', '+'
            ]:
                for i, nde in enumerate(nodes):
                    nde['host'] = socket.gethostbyname(nde['host'])
                    nodes[i] = nde

            # Conectamos no REDIS
            conexao_redis_cluster = StrictRedisCluster(
                startup_nodes=nodes,
                skip_full_coverage_check=True,  # Config necessária para uso do AWS ElastiCache
                decode_responses=True)

            # Retornamos, torcendo que tenhamos conectado
            return conexao_redis_cluster
        else:
            return None

    except KeyError:
        return None

def inicializa_conexao_mongo(configuracoes):
    try:
        if configuracoes.has_option("MONGO", "uri"):
            uri_mongo = configuracoes.get("MONGO", "uri")
            return motor.motor_tornado.MotorClient(
                uri_mongo,
                appname=(configuracoes.getnone('TORNADO', 'nome') if hasattr(configuracoes, 'getnone') else None)
            )

        elif configuracoes.has_option("MONGO", "host"):
            host = configuracoes.get("MONGO", "host")
            if configuracoes.has_option("MONGO", "porta"):
                porta = int(configuracoes.get("MONGO", "porta"))
            else:
                porta = 27017
            return motor.motor_tornado.MotorClient(
                host, porta,
                appname=(configuracoes.getnone('TORNADO', 'nome') if hasattr(configuracoes, 'getnone') else None)
            )
    except KeyError as kerr:
        raise Handler_Error(f"Arquivo de configuracoes nao contem configuracoes MONGO - {kerr}")

def inicializa_conexoes(configuracoes):
    return {
        'conexao_redis_single': inicializa_conexao_redis_single(configuracoes),
        'conexao_redis_cluster': inicializa_conexao_redis_cluster(configuracoes),
        'conexao_mongo_raiz': inicializa_conexao_mongo(configuracoes)
    }

def finaliza_conexoes(dict_conexoes):
    if 'conexao_redis_single' in dict_conexoes:
        del dict_conexoes['conexao_redis_single']
    if 'conexao_redis_cluster' in dict_conexoes:
        del dict_conexoes['conexao_redis_cluster']
    if 'conexao_mongo_raiz' in dict_conexoes:
        if hasattr(dict_conexoes['conexao_mongo_raiz'], 'close'):
            dict_conexoes['conexao_mongo_raiz'].close()

class Handler_Error(Exception):
    def __init__(self, string, *args, **kwargs):
        super(Handler_Error, self).__init__("{}".format(string))
        self.argumentos_posicionais = args
        self.argumentos_nominais = kwargs

