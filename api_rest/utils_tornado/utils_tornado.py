#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
# IMPORTS
"""


import os
import sys
import json
import signal
import logging

# Dependencias Tornado
import tornado.web
import tornado.gen
import tornado.ioloop
import tornado.options

# Dependencias MXT
from .utils import le_configuracoes, inicializa_conexoes, finaliza_conexoes


"""
# CONFIG
"""


CONFIG_INTERNA_TORNADO = {'xheaders': True}


"""
# PAYLOAD
"""


class ServidorTornado(object):

    def __init__(self, mapa_endpoints, arquivo_configuracoes, **kwargs):
        if os.sep not in arquivo_configuracoes:
            arquivo_configuracoes = os.getcwd() + os.sep + arquivo_configuracoes
        logging.warning("Preparando o servidor TORNADO com as configuracoes: {}".format(arquivo_configuracoes))

        if len(sys.argv) < 2:
            raise AttributeError("O primeiro argumento da linha de comando deve ser o arquivo de configuracoes!")

        # Parseamos as configuracoes
        tornado.options.parse_command_line(sys.argv[2:])
        self.config = le_configuracoes(arquivo_configuracoes)

        # Inicializamos o nível de LOG
        try:
            self.nivel_log = self.config.get("TORNADO", "nivel_log")
        except KeyError:
            self.nivel_log = 'INFO'

        # Inicializamos as conexoes com os bancos de dados
        self.conexoes_bancos_de_dados = inicializa_conexoes(self.config)

        # Inicializamos o contexto dos endpoints
        self.contexto = {"config": self.config, "nivel_log": self.nivel_log}
        self.contexto.update(self.conexoes_bancos_de_dados)

        # Inicializamos a lista de endpoints
        self.lista_endpoints = []

        # Inserimos os endpoints sem autenticação no contexto
        try:
            endpoints_sem_auth = json.loads(self.config.get("TORNADO", "endpoints_sem_auth"))
        except KeyError:
            endpoints_sem_auth = []

        self.contexto.update({"endpoints_sem_auth": endpoints_sem_auth})

        # Para cada endpoint e sua classe handler
        for endpoint, classe in mapa_endpoints.items():

            # Incluimos na lista o objeto tupla do endpoint, já incluindo o contexto
            self.lista_endpoints.append((r"{}".format(endpoint), classe, self.contexto))

        # Inicializamos a aplicação tornado
        self.app_tornado = tornado.web.Application(self.lista_endpoints)
        # Monitoramos o sistema operacional por um sinal de TERMINO
        # Caso no qual executamos o método de interrupção do tornado
        signal.signal(signal.SIGTERM, self.desliga_servidor)

        # Monitoramos o sistema operacional por um sinal de INTERRUPÇÃO
        # Caso no qual executamos o método de interrupção do tornado
        signal.signal(signal.SIGINT, self.desliga_servidor)

    def inicia_servidor(self, texto_subclasse=''):

        # Mapeamos a aplicação do servidor Tornado para a porta desejada
        try:
            porta = self.config.getint("TORNADO", "porta")
        except:
            if '--porta' in sys.argv:
                pos = sys.argv.index('--porta')
                porta = int(sys.argv[pos+1])
            else:
                raise KeyError("Nao foi possivel identificar a PORTA do servidor!")
        self.app_tornado.listen(porta, **CONFIG_INTERNA_TORNADO)

        # Iniciamos o servidor tornado
        logging.warning(
            "[{}]: Iniciando o Servidor{} {}!".format(porta, texto_subclasse, self.config.get("TORNADO", "nome")))

        # Executamos o comando de início do tornado
        tornado.ioloop.IOLoop.instance().start()

    def desliga_servidor(self, sinal, frame):
        u"""
        Função que desliga o servidor tornado em execução
        @return: None
        """

        # Logamos a parada iminente
        logging.warning("Recebido sinal {}! Parando o Servidor {}!".format(sinal, self.config.get("TORNADO", "nome")))

        # Executamos o comando de finalizar o tornado
        tornado.ioloop.IOLoop.instance().stop()

        # Finalizamos as conexões com os bancos de dados
        finaliza_conexoes(self.conexoes_bancos_de_dados)

        # Lançamos o log final
        logging.warning("Servidor {} desligado!".format(self.config.get("TORNADO", "nome")))

        # Desligamos o processo
        sys.exit(0)
