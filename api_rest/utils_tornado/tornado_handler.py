from tornado.web import RequestHandler
import json
import logging
import sys
import traceback
from tornado.log import LogFormatter
import jwt


class jvHandler(RequestHandler):

    """
    Sobrescrita do método initialize do RequestHandler
    """
    def initialize(self, **kwargs):
        # Copiamos completament os kwargs e configurações de setup
        self.kwargs = kwargs
        self.config = self.kwargs['config']

        # Criamos um alias para os kwargs, o "contexto"
        self.contexto = self.kwargs

        # Recuperamos a conexão com o REDIS SINGLE
        self.conexao_redis_single = kwargs['conexao_redis_single']

        # Recuperamos a conexão com o REDIS CLUSTER
        self.conexao_redis_cluster = kwargs['conexao_redis_cluster']

        # Recuperamos a conexão com o MONGO
        self.conexao_mongo_raiz = kwargs['conexao_mongo_raiz']

        self.database = self.config.get("API", "database_autenticacao")

        # Definimos a database e collection de log de erros
        try:
            self.database_log_erros = self.config.get("TORNADO", "database_log_erros")
        except:
            self.database_log_erros = 'LOG_ERROS'
        try:
            self.colecao_log_erros = self.config.get("TORNADO", "colecao_log_erros")
        except:
            self.colecao_log_erros = 'LOG_ERROS'

        self.classe = str(self.__class__.__name__)
        self.nome_servidor = self.config.get("TORNADO", "nome")

        # Inicializamos o logger da classe
        tag = "{}".format(self.__class__.__name__)
        self.logger = logging.getLogger(tag)
        self.logger.propagate = False
        channel = logging.StreamHandler()
        channel.setFormatter(LogFormatter(
            fmt='%(color)s[%(levelname)1.1s %(asctime)s]%(end_color)s [' + tag + '] %(message)s'))
        self.logger.handlers = []
        self.logger.addHandler(channel)
        self.logger.setLevel(self.contexto.get('nivel_log', 'INFO'))
        self.tags_string = ""

        self.decode_token = {}
        self.formt_token = ""
        self.token = ""

    def set_default_headers(self):
        self.add_header("Access-Control-Allow-Origin", "*")
        self.add_header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS")
        self.add_header("Content-Type", "application/json")
        self.add_header("Access-Control-Allow-Headers",
                        "x-requested-with,contentType,Content-Type,authorization,Authorization")

    async def prepare(self):

        try:
            auth = await self.autenticacao()
            assert auth, "Usuário não autenticado"

        except jwt.ExpiredSignatureError as exp:
            chave_privada = self.config.get("API", "private_key")
            # Se o token estiver expirado, fazemos mesmo assim o decode para possível uso
            self.decode_token = jwt.decode(self.token, chave_privada, verify=False)
            self.log(str(exp))
            # Usuario identificado, porém resposta negada devido ao token expirado (status forbidden)
            self.set_status(403)
            self.write(json.dumps({
                "msg": str(exp)
            }))
            self.finish()

        except AssertionError as ass:
            self.log(str(ass))
            # Usuário não identificado, portanto status unauthorized
            self.set_status(401)
            self.write(json.dumps({
                "msg": str(ass)
            }))
            self.finish()

        except Exception as exp:
            self.log_err(str(exp))
            self.set_status(500)
            self.write(json.dumps({
                "msg": str(exp)
            }))
            self.finish()

    def recupera_token(self):
        # Recuperamos o token no get
        authorization = self.request.headers.get('Authorization')

        if authorization is None:
            raise Exception("Sem header [Authorization]")
        print("DEBUG", "authorization: {}".format(authorization))

        # Recuperamos os tokens
        authorization = authorization.split(" ")

        if len(authorization) < 2:
            raise Exception("Header [Authorization] invalido")
        print("DEBUG", "Tokens: {} {}".format(authorization[0], authorization[1]))

        formt_token = authorization[0]
        token = authorization[1]
        return formt_token, token

    async def autenticacao(self):

        # Recuperamos a lista de endpoints que não precisam de autenticação
        try:
            endpoints_sem_auth = json.loads(self.config.get("API", "endpoints_sem_auth"))
        except KeyError:
            endpoints_sem_auth = []
        # Recuperamos o endpoint da chamada atual
        uri = self.request.uri
        # Se a uri necessitar autenticação
        if uri not in endpoints_sem_auth:
            self.formt_token, self.token = self.recupera_token()
            if self.formt_token.lower() != "bearer":
                return False

            sessao = await self.conexao_mongo(self.database).tokens.find_one({
                "token": self.token,
                "login": True
            })

            if not sessao:
                return False
            chave_privada = self.config.get("API", "private_key")
            self.decode_token = jwt.decode(self.token, chave_privada)
            return True
        return True




    def conexao_mongo(self, database=""):
        if database == "":
            return self.conexao_mongo_raiz
        else:
            if self.conexao_mongo_raiz is None:
                return None
            else:
                return self.conexao_mongo_raiz[database]

    def conexao_redis(self, usar_redis_single=False):

        if usar_redis_single:
            return self.conexao_redis_single

        else:

            # Caso não TENHAMOS uma conexão com o REDIS CLUSTER
            if self.conexao_redis_cluster is None:
                return self.conexao_redis_single

            else:
                return self.conexao_redis_cluster

    def instancia_logger(self, nivel):

        # Selecionamos o objeto de LOG
        if nivel == "DEBUG":
            objeto_log_local = self.logger.debug
        elif nivel == "INFO":
            objeto_log_local = self.logger.info
        elif nivel == "WARNING":
            objeto_log_local = self.logger.warning
        elif nivel == "ERROR":
            objeto_log_local = self.logger.error
        elif nivel == "APP_DEFAULT":
            return self.instancia_logger(self.contexto.get('nivel_log', 'INFO'))
        else:
            raise Exception("Nivel de LOG inexistente! As opcoes sao: DEBUG, INFO, WARNING e ERROR")
        return objeto_log_local

    def log(self, texto, nivel="APP_DEFAULT"):

        try:
            objeto_log_local = self.instancia_logger(nivel)
            # Formatamos e escrevemos a mensagem
            mensagem = '{} : {}'.format(self.tags_string, texto)
            objeto_log_local(mensagem)

        except Exception as exp:
            self.logger.error("Erro no LOG:")
            self.logger.error(exp)
            self.logger.error("Mensagem que gerou erro no LOG:")
            self.logger.error(texto)
            self.logger.error("Traceback do erro no LOG:")
            self.logger.error(traceback.format_exc())

    def log_err(self, texto):
        self.addLogTag("ERRO")
        trace = str(traceback.format_exc())
        # print(trace)
        self.log(trace, nivel='ERROR')
        self.log(texto, nivel='ERROR')

        dict_dados = {
            'headers': self.request.headers if hasattr(self, 'request') else None,
            'body': self.request.body if hasattr(self, 'request') else None,
        }

        try:
            self.conexao_mongo(self.database_log_erros)[self.colecao_log_erros].insert_one({
                'tipo_servidor': 'TORNADO',
                'nome_servidor': self.nome_servidor,
                'classe': self.classe,
                'mensagem_erro': str(texto),
                'stack_trace': trace.splitlines() if isinstance(trace, str) else None,
                # 'data_hora_salvamento_int': agora_int,
                # 'data_hora_salvamento_str': agora_str,
                'log_tags': self.tags_string,
                'python': str(sys.version_info),
                'dados': dict_dados,
            })
        except Exception as exp:
            self.log("Erro ao salvar log de erros no mongo", nivel='ERROR')


    def addLogTag(self, tag):
        self.tags_string = f"[{str(tag)}]"
