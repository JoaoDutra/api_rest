from api_rest.utils_tornado.tornado_handler import jvHandler
class Ping(jvHandler):
    u"""
    Classe que implementa um RequestHandler do tornado, respondendo um PING.
    """

    async def get(self):
        u"""
        Método que processa uma request HTTP GET recebida pelo servidor tornado
        """

        # Setamos o status funcional
        self.set_status(200)

        # Finalizamos a request
        self.finish()
