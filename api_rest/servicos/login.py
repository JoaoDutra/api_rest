from api_rest.utils_tornado.tornado_handler import jvHandler
import json
import hashlib
import datetime
import jwt
from api_rest import tools
class RequisicaoIncorreta(Exception):
    pass

class Login(jvHandler):
    async def post(self):
        try:
            body = json.loads(self.request.body)
            if body.get('email') and body.get('senha'):
                # Encoding da senha
                senha = hashlib.md5(body["senha"].encode()).hexdigest()
                body.update({
                    "senha": senha
                })
                # Buscamos o usuario
                usuario = await self.conexao_mongo(self.database).usuarios.find_one(body)
                assert usuario, "Usuário não encontrado!"
                exp = 5
                if not usuario['first_access']:
                    exp = 12*60
                token = await self.gera_token(usuario, exp=exp)
                usuario.pop("senha")
                usuario.pop("_id")
                usuario.update({
                    "token": token,
                    "ip_remoto": self.request.remote_ip,
                    "datahora": tools.formata_datahora("agora", formato_destino="timestamp"),
                    "datahora_str": tools.formata_datahora("agora"),
                    "datahora_str_sp": tools.formata_datahora("agora", fuso_destino="America/Sao_Paulo"),
                    "login": True
                })


                await self.conexao_mongo(self.database).tokens.insert_one(usuario)
                # Novamente damos pop no _id pois a função insert_one coloca o novo _id no dict
                usuario.pop("_id")
                self.set_status(200)
                self.write(json.dumps(usuario))
                self.finish()
            else:
                raise RequisicaoIncorreta("Requisição incorreta")


        except AssertionError as ass:
            self.log_err(str(ass))
            self.set_status(403)
            self.write(json.dumps({
                'msg': str(ass)
            }))
            self.finish()
        except RequisicaoIncorreta as ri:
            self.log_err(str(ri))
            self.set_status(400)
            self.write(json.dumps({
                'msg': str(ri)
            }))
            self.finish()
        except Exception as exp:
            self.log_err(str(exp))
            self.set_status(500)
            self.write(json.dumps({
                'msg': str(exp)
            }))
            self.finish()

    async def gera_token(self, dados_usuario, exp=5):
        private_key = self.config.get("API", "private_key")
        payload_encoded = {
            "payload": {
                "login": dados_usuario["email"],
                "name": dados_usuario["nome"],
                "ip_remoto": self.request.remote_ip
            },
            "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=exp)
        }

        token = jwt.encode(payload_encoded,
                           private_key,
                           algorithm='HS256').decode("UTF-8")
        return token
