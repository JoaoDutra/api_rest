from api_rest.utils_tornado.tornado_handler import jvHandler
import json


class Logout(jvHandler):
    async def post(self):
        try:
            self.formt_token, self.token = self.recupera_token()
            update = await self.conexao_mongo(self.database).tokens.update_one(
                {
                    "token": self.token
                },{
                    "$set": {
                        "login": False
                    }
                })
            if update.modified_count > 0:
                msg = "Logout realizado!"
            else:
                msg = "Sessão de usuário inexistente ou usuaário já deslogado!"

            self.set_status(200)
            self.write(json.dumps({
                'msg': msg
            }))
            self.finish()

        except Exception as exp:
            self.log_err(str(exp))
            self.set_status(500)
            self.write(json.dumps({
                'msg': str(exp)
            }))
            self.finish()
