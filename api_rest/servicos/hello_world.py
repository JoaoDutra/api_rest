from tornado.web import RequestHandler
import json
from api_rest.utils_tornado.tornado_handler import jvHandler
from api_rest import tools

import motor
class HelloWorld(jvHandler):
    async def get(self):
        try:
            self.addLogTag('IUPI')
            self.log(tools.formata_datahora("now"))
            self.set_status(200)
            self.write(json.dumps({
                'msg': 'Hello World',
                "decode": self.decode_token
            }))
        except Exception as exp:
            self.log_err(str(exp))
            self.set_status(500)
            self.write(json.dumps({
                'msg': str(exp)
            }))