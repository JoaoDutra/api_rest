import json
from api_rest.utils_tornado.tornado_handler import jvHandler


class Alive(jvHandler):

    async def get(self):
        self.set_status(200)
        self.write(json.dumps({"status": "ok", "response": "Instancia OK"}))
        self.finish()
